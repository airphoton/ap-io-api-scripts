# Python Script for AirPhoton API
This is a python script for easy use of the AirPhoton API. 
Script designed using Python 3.8


## Usage

> To use the script you may have to install the *requests* package. 
>
> >$ pip install requests

Run the script through terminal:
```
$ python apitool.py
```


## Extended Usage

While the Usage section above is enough to run the tool if you know what to do, some users requested a more detailed setup and usage example.

``` bash
# download project
git clone https://gitlab.com/airphoton/ap-io-api-scripts
cd ./ap-io-api-scripts

# setup a python3 virtual environment
python -m venv .env
source .env/bin/activate # for POSIX bash.See more examples at https://docs.python.org/3/library/venv.html

# once working the the venv, install packages
pip install -r requirements.txt

# use the program
python apitool.py --help # help to get started
```


## Logging In

Enter your username/password when prompted:
```
Enter username: <username>
Enter password for user user@aphoton.com: 
```
If user/pass is valid, *user* and *instrument* info will be displayed:
```
@get_user_url
https://sandbox.airphoton.io/v1/users/?username=user@airphoton.com
{
  "url": "https://sandbox.airphoton.io/v1/users/1/",
  "username": "user@airphoton.com",
  "email": "user@airphoton.com",
  "is_staff": true,
  "groups": [
    "https://sandbox.airphoton.io/v1/groups/1/"
  ]
}

@get_instrument_url
https://sandbox.airphoton.io/v1/instruments/?serial_number=SS5013
{
  "url": "https://sandbox.airphoton.io/v1/instruments/51e38515-3fb6-4f00-838b-7fea3cadc42f/",
  "serial_number": "SS5013",
  "name": "SS5013",
  "group": "https://sandbox.airphoton.io/v1/groups/1/",
  "status": "DEMO",
  "info": {}
}
```

If user/pass is invalid:
```
Invalid username/password
```


## Entering Commands
Enter a command when prompted:
```
@post_new_control
Enter a command (or "exit"):
```
After a command is entered (valid or invalid):
```
@post_new_control
Enter a command (or "exit"): echo Hello!
{
  "url": "https://sandbox.airphoton.io/v1/controls/316c9fdb-5019-4d15-b463-ea3a061f3935/",
  "instrument": "https://sandbox.airphoton.io/v1/instruments/51e38515-3fb6-4f00-838b-7fea3cadc42f/",
  "name": null,
  "status": "new",
  "created_by": "https://sandbox.airphoton.io/v1/users/7/",
  "command": "echo Hello!"
}

Control created: https://sandbox.airphoton.io/v1/controls/316c9fdb-5019-4d15-b463-ea3a061f3935/
Waiting for response   
Waiting for response.  
Waiting for response.. 
Waiting for response...
```
For a successful command, the Control Response will be:
```
Control Response received:
{
  "url": "https://sandbox.airphoton.io/v1/controlresponses/77871829-7285-453d-a36a-ca238105b566/",
  "control": "https://sandbox.airphoton.io/v1/controls/316c9fdb-5019-4d15-b463-ea3a061f3935/",
  "error": "",
  "response": {
    "text": "echo Hello!\r\n"
  }
}
```
For an invalid command:
```
Control Response received:
{
  "url": "https://sandbox.airphoton.io/v1/controlresponses/c44994e5-0cae-41ba-9d50-4f67aa9b4177/",
  "control": "https://sandbox.airphoton.io/v1/controls/c5a82b28-687a-4711-883b-1ab6e6e61178/",
  "error": "",
  "response": {
    "text": "unknown command:Hello!\r\n"
  }
}
```
> Note: Receiving a response might take a while. 
> The command goes from ``python -> server -> radio -> instrument -> radio -> server -> python`` and sometimes it takes a while for the instrument to checkin.



## Exiting

To exit, simply type 'exit':
```
Enter a command (or "exit"): exit
```



## Extra Information

Check the help information for some useful tools if you plan to use this tool repetitively: ``python apitool.py --help``

- ``--user`` to specify your username
- ``--instrument`` to specify the instrument serial number (``SS5013``, default)
- ``--hostname`` to specify production (``api``) or sandbox (``sandbox``, default) server
- ``--quite`` to limit the amount of output. Hides helper informational helper text, ``/users/`` json, ``/instruments/`` json, and ``/controls/`` json. The ``/controlresponses/`` json will always print.