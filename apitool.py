#Python 3.8
import argparse
import getpass
import json
import logging
import requests
import time

# global logger
gLogger = logging.getLogger('ap_api_script')

# for waiting animation
ellipsis = ["   ", ".  ", ".. ", "..."]

def get_user_url(hostname, user, password):
    gLogger.debug("\n@get_user_url")
    # https://sandbox.airphoton.io/v1/users/?username=[USERNAME]

    url = hostname + "/v1/users/?username="+user
    gLogger.debug(url)

    r = requests.get(url, auth=(user, password))

    if r.status_code == 200:
        gLogger.debug(json.dumps(r.json()['results'][0], indent=2))
        return r.json()['results'][0]
    else:
        gLogger.error(str(r.status_code) + " because " + str(r.json()))
        return {}

def get_instrument_url(hostname, instrument, user, password):
    gLogger.debug("\n@get_instrument_url")
    # https://sandbox.airphoton.io/v1/instruments/?serial_number=[INSTRUMENT]

    url = hostname + "/v1/instruments/?serial_number=" + instrument
    gLogger.debug(url)

    r = requests.get(url, auth=(user, password))

    if r.status_code == 200:
        gLogger.debug(json.dumps(r.json()['results'][0], indent=2))
        return r.json()['results'][0]
    else:
        gLogger.error(str(r.status_code) + " because " + str(r.json()))
        return {}

def post_new_control(hostname, instrument_rec, user_rec, user, password):
    gLogger.debug("\n@post_new_control")

    control_url = hostname + "/v1/controls/"
    # gLogger.debug(control_url)

    # Initialize data
    data = {}
    data["instrument"] = instrument_rec['url']
    data["created_by"] = user_rec['url']
    data["command"] = input("Enter a command (or \"exit\"): ")

    if (data["command"] == "exit"):
        gLogger.debug("Exit Requested")
        return False
    
    # POST the control
    r = requests.post(control_url, auth=(user, password), headers={'content-type':'application/json'}, json=data)

    if r.status_code == 201:
        gLogger.debug(json.dumps(r.json(), indent=2))
        return r.json()
    else:
        gLogger.error(str(r.status_code) + " because " + str(r.json()))
        return {}


def await_response(hostname, control_rec, user, password, count):
    print("Waiting for response" + ellipsis[count % 4], end="\r")

    # Check if the control response has been made
    url = hostname + "/v1/controlresponses/?control=" + control_rec["url"].rsplit('/')[-2]
    r = requests.get(url, auth=(user, password))
    
    if r.status_code == 200 and r.json()['results']:
        gLogger.info("@wait_reponse: counted to %s", count)
        # gLogger.debug(r.json()['results'][0])
        return r.json()['results'][0]
    pass


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--user',
            default="",
            help='Username for API server',
            dest="username")
    parser.add_argument('-n', '--host',
            default="sandbox",
            help="Hostname for the server (default: sandbox)",
            dest="hostname")
    parser.add_argument('-i', '--instrument',
            default="SS5013",
            help='Instrument Serialnumber (default: AirPhoton\'s SS5013)',
            dest="instrument")
    parser.add_argument('-q', '--quiet',
            default=False,
            help='Supress response messages (-qq for extra quiet)',
            action='count',
            dest='quiet')
    return parser.parse_args()

def check_user_exists(hostname, user, password):
    # utility function to make this script more user friendly
    url = hostname + "/v1/"
    print(url)
    r = requests.get(url, auth=(user, password))
    return ("detail" in r.json().keys())


if __name__ == "__main__":
    args = get_args()
    
    # setup logging
    ch = logging.StreamHandler()
    gLogger.addHandler(ch)

    if args.quiet == 0:
        gLogger.setLevel(logging.DEBUG)
    elif args.quiet >= 1:
        gLogger.setLevel(logging.INFO) 

    # prompt for username
    user = args.username
    if user == "":
        user = input("Enter username: ")
    else:
        gLogger.debug('Using username from cli: %s', user)

    # handle localhost testing
    if args.hostname == 'api' or args.hostname == 'sandbox':
        hostname = 'https://' + args.hostname + '.airphoton.io'
    else:
        gLogger.debug('Using hostname from cli: %s', args.hostname)
        hostname = args.hostname

    # prompt for password
    password = getpass.getpass(prompt="Enter password for user %s: " % user)

    # check for valid user/pass
    if (check_user_exists(hostname, user, password)):
        gLogger.warning("Invalid username/password")
        exit()

    # make user information API call
    user_rec = get_user_url(hostname, user, password)
    instrument_rec = get_instrument_url(hostname, args.instrument, user, password)
    
    while True:
        gLogger.info("----------------------------------------")
        # make a create Control API call
        control_rec = post_new_control(hostname, instrument_rec, user_rec, user, password)
        
        if control_rec:
            gLogger.warning("\nControl created: %s", control_rec["url"])
            
            # track response checks
            count = 0
            url = hostname + "/v1/controlresponses/?control=" + control_rec["url"].rsplit('/')[-2]

            # gLogger.debug(url)
            info = False
            while (not info):
                info = await_response(hostname, control_rec, user, password, count)
                
                # it takes time...
                # python -> server -> radio -> instrument -> radio -> server -> python
                time.sleep(5)
                count += 1

                if  info:
                    gLogger.info("\nControl Response received:")
                    gLogger.info(json.dumps(info, indent=2))
        else:
            exit()