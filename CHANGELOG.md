# Changelog

## 2021-02-08

### Added

- Extended Usage details to the `readme`

### Fixed

- Fixed the error where `hostname` was not cleaned up for proper usage.


## 2020-12-07

### Added

- _This_ changelog.
- Added logging of ControlResponse check counts once the response is received.

### Changed

- Updated how hostnames are handled so localhost testing can be easier.
- Updated all multi-part json() calls because the next api server release uses pagination on list views.

### Fixed

- Returned the Waiting for response... code to be print() so it can take up only one line.